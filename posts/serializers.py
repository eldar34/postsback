from rest_framework import serializers

from .models import Posts

class PostsListSerializer(serializers.ModelSerializer):
    """ Get Post List """

    class Meta:
        model = Posts
        fields = "__all__"