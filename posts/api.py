from rest_framework import viewsets, renderers, permissions

# Create your views here.

from .models import Posts
from .serializers import PostsListSerializer


class PostReadViewSet(viewsets.ModelViewSet):
    queryset = Posts.objects.all()
    permission_classes=[permissions.AllowAny]
    serializer_class = PostsListSerializer

    def perform_destroy(self, instance):
        storage, path = instance.poster.storage, instance.poster.path
        instance.delete()
        storage.delete(path)
   
        
        