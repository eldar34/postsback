from django.urls import path, include
from . import views, api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'api/v1/posts', api.PostReadViewSet, basename='posts_api')

urlpatterns = [
    path('', views.PostCreate.as_view(), name='posts'),
    path('posts/', views.PostList.as_view(), name='posts_list'),
    path('posts/delete/<int:pk>', views.PostDelete.as_view(), name='posts_delete'),
]

urlpatterns += router.urls