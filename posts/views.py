from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView
from django.shortcuts import render
from django.views import View
from django.urls import reverse_lazy

# Create your views here.

from .models import Posts

class PostCreate(CreateView):
    model = Posts
    fields = ['title', 'description', 'content', 'poster'] 
    template_name_suffix = '_create_form'

    def form_valid(self, form):
        
        return super().form_valid(form)

class PostList(View):
    def get(self, request):
        posts = Posts.objects.all()
        return render(request, 'posts/posts_list.html', {'posts': posts})

class PostDelete(DeleteView):
    model = Posts
    success_url = reverse_lazy('posts_list')
    template_name_suffix = '_delete_form'

    def delete(self, request, *args, **kwargs):
        post = self.get_object()
        storage, path = post.poster.storage, post.poster.path
        storage.delete(path)
        return super().delete(self, request, *args, **kwargs)
