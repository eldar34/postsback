from django.db import models
from datetime import date
from django.urls import reverse

# Create your models here.

class Posts(models.Model):
    title = models.CharField(max_length=150)
    description = models.CharField(max_length=250)
    created_at = models.DateField(default=date.today)
    draft = models.BooleanField(default=False)
    content = models.TextField()
    poster = models.ImageField(upload_to="posters/", blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("posts", )
    
    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"
